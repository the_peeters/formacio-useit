<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Page Title</title>
        <style type="text/css" >
           ul{
             padding: 0;
             list-style: none;
           }
          #color0{
            color: red;
          }
          #color1{
            color: blue;
          }
          #color2{
            color: orange;
          }
          #color3{
            color: white;
            background-color: green;
          }
        </style>
    </head>
    <body>
      <h1>PHP</h1>
      <h2>Arrays</h2>

      <?php
        $arr = array("A", "B", "C","D"); 

        echo "<ul>";
        for ($i=0; $i<count($arr);$i++){
            echo "<li>".($i+1).". $arr[$i]</li>";
        }
        echo "</ul>";


        $data = [ 
          0 => [
          "brand" => "Audi",
          "model" => "A3",
          "color" => "red"
          ],
          1 => [
            "brand" => "Audi",
            "model" => "A4",
            "color" => "blue"
          ],
          2 => [
            "brand" => "Seat",
            "model" => "Ibiza",
            "color" => "orange"],
          3 => [
            "brand" => "BMW",
            "model" => "Serie 1",
            "color" => "green",
          ],
        ];
        print_table_html($data);


        function print_table_html($dictionary_array) {
          echo "<table>";
          echo table_header($dictionary_array);
          echo table_body($dictionary_array);
          echo "</table>";
          
        } 
        function table_header($dictionary_array) {
          echo "<thead><tr>";
          echo "<th>#</th>";
          foreach($dictionary_array[0] as $k => $v) {
            echo "<th>$k</th>";
          }
          echo "</tr></thead>";
        } 
        function table_body($dictionary_array) {
          echo "<tbody>";
          for ($i=0; $i<count($dictionary_array);$i++){
            echo "<tr>";
            echo "<td>".($i+1)."</td>";

            foreach($dictionary_array[$i] as $k => $v) {
              if ($k=="color"){
                echo  "<td id=\"$k$i\">".$v."</td>";
              }else{
                echo  "<td>".$v."</td>";
              }
              
            }
            echo "</tr>";
            
          }
          echo "</tbody>";
        }
        function assign_color_($key,$value){
          return "<td>".$v."</td>";
        }
      ?>
      <p><a href="formularis_MySQL.html">Anar al formulari</a></p>
      <p><a href="/">Inici</a></p>
    </body>
</html>
